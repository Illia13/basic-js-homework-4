"use strict";

// let num1, num2;

// while (true) {
//   num1 = prompt("Введіть перше число:");
//   num2 = prompt("Введіть друге число:");

//   if (
//     (num1 !== null && num1 !== "" && !isNaN(num1),
//     num2 !== null && num2 !== "" && !isNaN(num2))
//   ) {
//     break;
//   } else {
//     alert("Будь ласка, введіть числа.");
//   }
// }

// let minNum = Math.min(num1, num2);
// let maxNum = Math.max(num1, num2);

// console.log(`Цілі числа від ${minNum} до ${maxNum}:`);
// for (let i = minNum; i <= maxNum; i++) {
//   console.log(i);
// }

let userNumber;

while (true) {
  userNumber = prompt("Введіть число:");

  if (!isNaN(userNumber) && userNumber % 2 === 0) {
    console.log(`Ви ввели парне число: ${userNumber}`);
    break;
  } else {
    alert("Будь ласка, введіть парне число.");
  }
}
